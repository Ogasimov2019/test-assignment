<?php

require('../db_connection.php');

include "product.php";

include "dvd.php";
include "book.php";
include "furniture.php";

if (isset( $_POST))
{
    if(count($_POST) !== 0)
    {
        $sku = $_POST['sku'];
        $sku = strtoupper($sku);
        
        $sql = $conn->prepare("SELECT * FROM products WHERE sku=?"); //check if sku exists
        $sql->bind_param("s", $sku);
        $sql->execute();
        $result = $sql->get_result();
 
        if($result->num_rows > 0)
        {
            header("refresh:0;url=add_product.php"); 
            echo "<script>alert('This SKU value - ".$sku." already exists!')</script>"; 
        }
        else
        {
            $products = array("DVD"=>new DVD(), "Book"=>new Book(), "Furniture"=>new Furniture());
            
            $name = $_POST['product_name'];
            $price = doubleval($_POST['price']);
            
            $product_type = $_POST['typeOfProduct'];
            
            $special_attribute = "";
            
            //loop through all posted fields
            foreach($_POST as $key => $value) {
                    
                //ignore the ones that we used
                if($key == 'sku' || $key == 'product_name' || $key == 'price' || $key == 'typeOfProduct')
                {
                    continue;
                }
                else
                {
                    $special_attribute .= $value.'x'; //we concatenate all value(s) and add 'x' character to the end
                }    
                    
            };
                
            //get rid off the last character - x   which was added in the loop
            $special_attribute = substr($special_attribute, 0, strlen($special_attribute) - 1);
                
            //create product object
            $product = $products[$product_type];
            $product->set_sku($sku);
            $product->set_name($name);
            $product->set_price($price);
            $product->set_specific_attribute($special_attribute);
            
            # insert values to the database
            $query = $conn->prepare("INSERT INTO products (sku, name_of_product, price_of_product, special_attribute) VALUES(?, ?, ?, ?)");
            $query->bind_param("ssds", $sku_, $name_, $price_, $specific_attribute_);

            $sku_ = $product->get_sku();
            $name_ = $product->get_name();
            $price_ = $product->get_price();
            $specific_attribute_ = $product->get_specific_attribute();
            
            $query->execute();
            $query->close();
        }
        
    }

}

$conn->close();

header('Location: index.php');