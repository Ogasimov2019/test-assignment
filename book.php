<?php

class Book extends Product {
  
  private $weight;
  
  public function set_specific_attribute($val)
  {
      $this->weight = "Weight: ". $val ." KG";
  }
  
  public function get_specific_attribute()
  {
    return $this->weight;
  }
}