function GetAttr() {
        
        var products = {
            
            'DVD':Get_DVD_attributes(), 
            'Book':Get_Book_attributes(), 
            'Furniture':Get_Furniture_attributes()
            
        };
        
        var special_attr = document.getElementById("specialAttributes");
    
        //remove all child elements if any
        while (special_attr.firstChild) {
            special_attr.removeChild(special_attr.lastChild);
        }
    
        var chosenType = document.getElementById("typeOfProduct").value;
        var div = products[chosenType];
      
        special_attr.appendChild(div);
    }
    
function Get_DVD_attributes()
{
    var div = document.createElement("div");
    div.className = 'form-group';
      
    var label = document.createElement("label");
    label.setAttribute("for", "size");
    label.innerHTML = 'Size (MB)';
    
    var desc = document.createElement("p");
    desc.setAttribute('style','margin-top:10px');
    desc.innerHTML = 'Please, provide size (MB) of DVD product';
    
    var input = document.createElement("input");
    input.setAttribute('type', 'text');
    input.setAttribute('class', 'form-control');
    input.setAttribute('required','');
    input.setAttribute('id', 'size');
    input.setAttribute('name', 'size');
            
    div.append(label, input, desc);
    
    return div;
}

function Get_Book_attributes()
{
    var div = document.createElement("div");
    div.className = 'form-group';
      
    var label = document.createElement("label");
    label.setAttribute("for", "weight");
    label.innerHTML = 'Weight (Kg)';
    
    var desc = document.createElement("p");
    desc.setAttribute('style','margin-top:10px');
    desc.innerHTML = 'Please, provide weight (Kg) of book';
    
    var input = document.createElement("input");
    input.setAttribute('type', 'text');
    input.setAttribute('class', 'form-control');
    input.setAttribute('required','');
    input.setAttribute('id', 'weight');
    input.setAttribute('name', 'weight');
            
    div.append(label, input, desc);
    
    return div;
}

function Get_Furniture_attributes()
{
    var div = document.createElement("div");
    div.className = 'form-group';
      
    var labelHeight = document.createElement("label");
    labelHeight.setAttribute("for", "height");
    labelHeight.innerHTML = 'Height (CM)';
    
    var labelWidth = document.createElement("label");
    labelWidth.setAttribute("for", "width");
    labelWidth.innerHTML = 'Width (CM)';
    labelWidth.setAttribute('style','margin-top:10px');
    
    var labelLength = document.createElement("label");
    labelLength.setAttribute("for", "length");
    labelLength.innerHTML = 'Length (CM)';
    labelLength.setAttribute('style','margin-top:10px');
            
    var desc = document.createElement("p");
    desc.setAttribute('style','margin-top:10px');
    desc.innerHTML = 'Please, provide height (cm), width (cm) and length (cm) of furniture';
    
    var inputHeight = document.createElement("input");
    inputHeight.setAttribute('type', 'text');
    inputHeight.setAttribute('class', 'form-control');
    inputHeight.setAttribute('required','');
    inputHeight.setAttribute('id', 'height');
    inputHeight.setAttribute('name', 'height');
            
    var inputWidth = document.createElement("input");        
    inputWidth.setAttribute('type', 'text');
    inputWidth.setAttribute('class', 'form-control');
    inputWidth.setAttribute('required','');
    inputWidth.setAttribute('id', 'width');
    inputWidth.setAttribute('name', 'width');        
            
    var inputLength = document.createElement("input");
    inputLength.setAttribute('type', 'text');
    inputLength.setAttribute('class', 'form-control');
    inputLength.setAttribute('required','');
    inputLength.setAttribute('id', 'length');
    inputLength.setAttribute('name', 'length');        
            
    div.append(labelHeight, inputHeight, labelWidth, inputWidth, labelLength, inputLength, desc);
    
    return div;
}
    
function validateInputFields()
{
        var sku = document.forms["product-form"]["sku"].value;
        var name = document.forms["product-form"]["product_name"].value;
        var price = document.forms["product-form"]["price"].value;
        
        sku = sku.toLowerCase();
        
        
        if(/^[a-z0-9]+$/.test(sku))
        {
            if((/^([a-zA-Z]+\s?)+$/).test(name))
            {
                if(/^[0-9]+(\.[0-9]{1,})?$/.test(price))
                {
                    
                    var productType = document.getElementById("typeOfProduct").value;
                    
                    //if special attribute is selected
                    if(productType != 'Type Switcher')
                    {
                        var i;
            
                        //get all input fields
                        var total = document.forms['product-form'].getElementsByTagName("input");
                        
                        //start iterating from new added ones (special attributes)
                        for(i = 3; i < total.length; i++ )
                        {
                            var val = total[i].value;
                            
                            if(!(/^\d+$/.test(val)))
                            {
                                alert("The field must contain a valid number!");
                                return false; 
                            }
                        }  
                    
                        return true;
                    }
                    else
                    {
                        alert("Please, define the special attribute of a product!");
                        return false; 
                    }
                    
                }
                else
                {
                    alert("Price contains invalid characters!");
                    return false;    
                }
            }
            else
            {
                alert("Product name contains invalid characters!");
                return false;    
            }   
        }
        else
        {
            alert("SKU contains invalid characters!");
            return false;    
        }
       
}
