<?php require('../db_connection.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Test Products</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
<style>

.topnav {
  overflow: hidden;
  background-color: #333;
}

#main {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 25px;
  text-decoration: none;
  font-size: 25px;
}

.topnav-right {
  float: right;
  padding: 14px 16px;
  font-size: 17px;
}
</style>
    
    </head>
    <body>
        
        
    <?php
        
        $query = "SELECT * FROM products";
        $result = $conn->query($query);
        
    ?>
        
        
        <div class="topnav">
        
          <a href="index.php" id="main">Product List</a>
          
          <div class="topnav-right">
            <a class="btn btn-success" href="add_product.php">Add</a>
            <input class="btn btn-danger" type="submit" form='deleteProductForm' value="Mass Delete">
          </div>
        
        </div>
        
        
        <div class="container">
          
                <?php
                    if ($result->num_rows > 0) {
                ?>
                    <ul class="list-inline">   
                    <form method='post' action='delete_product.php' id='deleteProductForm'>
                <?php        
                        while($row = $result->fetch_assoc()) {
                ?>
                
                     <li class="list-inline-item" style="width: 250px; margin: 20px 12px; padding:20px; border: 1px solid black;">
                                
                        <div class="form-check">
                            <input type="checkbox" name='check_products[]' class="form-check-input" value="<?= $row['id']; ?>">
                        </div>
                        
                        <div style="text-align:center;">
                            <p><?= $row['sku']; ?><p>
                            <p><?= $row['name_of_product']; ?></p>
                            <p><?= $row['price_of_product']; ?> $</p>
                            <p><?= $row['special_attribute']; ?></p>
                        </div>
                                
                    </li>
                 
                <?php 
                        }
                ?>
                    </form>
                    </ul>
                    
                <?php
                
                    }
                    else 
                    {
                      echo "<h4>No product was found.</h4>";
                    }
                ?>
        </div>
        
    </body>
</html>