<?php

class Furniture extends Product {
  
  private $dimension;
  
  public function set_specific_attribute($val)
  {
      $this->dimension = "Dimension: ". $val;
  }
  
  public function get_specific_attribute()
  {
    return $this->dimension;
  }
}