<?php

abstract class Product {
    
  private $sku;
  private $name;
  private $price;
  
  public function set_sku($sku)
  {
     $this->sku = $sku;
  }
  
  public function get_sku()
  {
     return $this->sku;
  }
  
  public function set_name($name)
  {
     $this->name = $name;
  }
  
  public function get_name()
  {
     return $this->name;
  }
  
  public function set_price($price)
  {
    $this->price = $price; 
  }
  
  public function get_price()
  {
     return $this->price;
  }
  
  abstract public function set_specific_attribute($val);
  abstract public function get_specific_attribute();
}