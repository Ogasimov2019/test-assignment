<?php

class DVD extends Product {
  
  private $size;
  
  public function set_specific_attribute($val)
  {
      $this->size = "Size: ". $val ." MB";
  }
  
  public function get_specific_attribute()
  {
    return $this->size;
  }
  
}