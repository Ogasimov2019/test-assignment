<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Test Products</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
<style>

.topnav {
  overflow: hidden;
  background-color: #333;
}

#main {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 25px;
  text-decoration: none;
  font-size: 25px;
}

.topnav-right {
  float: right;
  padding: 14px 16px;
  font-size: 17px;
}
</style>
    
    </head>
    <body>

        <div class="topnav">
        
          <a href="add_product.php" id="main">Product Add</a>
          
          <div class="topnav-right">
            <button class="btn btn-primary" type="submit" form="product-form">Save</button>
            <a href="index.php" class="btn btn-warning">Cancel</a>
          </div>
        
        </div>
        
        
        <div class="container">
          
          <form action="new_product.php" id="product-form" style="margin-top:50px;" method="post" onsubmit="return validateInputFields()" name="product-form">
              
            <div class="form-group">
              <label for="sku">SKU:</label>
              <input type="text" class="form-control" id="sku" placeholder="SKU" name="sku" required>
            </div>
            
            <div class="form-group">
              <label for="product_name">Name:</label>
              <input type="text" class="form-control" id="product_name" placeholder="Product Name" name="product_name" required>
            </div>
            
            <div class="form-group">
              <label for="price">Price($):</label>
              <input type="text" class="form-control" id="price" placeholder="Product Price" name="price" required>
            </div>
            
            <div class="form-group">
                
              <label for="typeOfProduct">Type Switcher</label>
              <select name="typeOfProduct" id="typeOfProduct" class="form-control" onchange="GetAttr()">
                <option selected>Type Switcher</option>
                <option value="DVD">DVD</option>
                <option value="Book">Book</option>
                <option value="Furniture">Furniture</option>
              </select>
              
            </div>
            
            
            <div id="specialAttributes"></div>
            
          </form>
          
        </div>

<script src="custom.js"></script>
        
    </body>
</html>